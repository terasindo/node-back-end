'use strict';

/**
 * Start up script
 */

let dotenv = require('dotenv');

// Load environment variable, AS EARLY AS POSSIBLEEEE
if (!process.env.DOTENV_PATH) {
  console.warn('No explicit .env path is configured, used .env in the root project.');
}
dotenv.config({path: process.env.DOTENV_PATH});
dotenv.load();
let _ = require('lodash');
let BPromise = require('bluebird');
let router = require('route-label');
let fs = require('fs-extra');
let path = require('path');

// Globals setup
global.appRoot = path.resolve(__dirname);
BPromise.promisifyAll(fs);
router.setBaseUrl(process.env.BASE_URL);

let logger = require('./utils/logger')(__filename);
let constants = require('./utils/constants');
// let cache = require('./utils/cache');

let isOn = false;

module.exports.on = function () {
  if (isOn) {
    return BPromise.resolve();
  }

  isOn = true;
  logger.verbose('Turning application on');

  let waitingTimeDate = process.env.WAITING_TIME_DATE || getNewestDate(constants.FOLDER.AGGREGATED, constants.EXTENSION.WAITING_TIME);
  let travelTimeDate = process.env.TRAVEL_TIME_DATE || getNewestDate(constants.FOLDER.AGGREGATED, constants.EXTENSION.TRAVEL_TIME);

  let graphFile = path.join(global.appRoot, constants.FILE.GRAPH);
  let waitingTimeFile = path.join(global.appRoot, constants.FOLDER.AGGREGATED, waitingTimeDate + constants.EXTENSION.WAITING_TIME);
  let travelTimeFile = path.join(global.appRoot, constants.FOLDER.AGGREGATED, travelTimeDate + constants.EXTENSION.TRAVEL_TIME);
  let travelPathFile = path.join(global.appRoot, constants.FILE.TRAVEL_PATH);
  let waitingTimeAutocompleteFile = path.join(global.appRoot, constants.FILE.WAITING_TIME_AUTOCOMPLETE);

  logger.info('Initializing C++ Black Box');
  logger.info('Using waiting time file %s', waitingTimeFile);
  logger.info('Using travel time file %s', travelTimeFile);
  logger.info('Using travel path file %s', travelPathFile);
  logger.info('Using waiting time autocomplete file %s', waitingTimeAutocompleteFile);

  let cbbInitStatus = require('./cbb/build/Release/cbb').init({
    GRAPH_FILE: graphFile,
    WAITING_TIME_FILE: waitingTimeFile,
    TRAVEL_TIME_FILE: travelTimeFile,
    TRAVEL_PATH_FILE: travelPathFile,
    WAITING_TIME_AUTOCOMPLETE_FILE: waitingTimeAutocompleteFile,
    NODE_ENV: process.env.NODE_ENV
  });

  if (!cbbInitStatus) {
    throw new Error('Error on initializing C++ Black Box');
  }

  return require('./modules/graph').init()
    .then(require('./modules/waitingTime').init)
    .then(require('./modules/travelTime').init)
    .then(require('./modules/travelPath').init);
};

module.exports.off = function () {
  if (!isOn) {
    return BPromise.resolve();
  }

  isOn = false;
  logger.verbose('Turning application off');
  return BPromise.resolve();
};

function getNewestDate(folderPath, fileExt) {
  // Get latest date
  return _.chain(fs.readdirSync(folderPath))
    .filter(function (filename) {
      return _.endsWith(filename, fileExt);
    })
    .map(function removeExtension(filename) {
      return filename.split('.')[0];
    })
    .sort()
    .last()
    .value();
}