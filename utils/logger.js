'use strict';

var _ = require('lodash');
var winston = require('winston');
var moment = require('moment');
var request = require('request');
var winstonDailyRotateFileTransport = require('winston-daily-rotate-file');
var util = require('util');

// Default levels from npm
var LEVEL = {
  error: 0,
  warn: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5
};

// Format the log
var FORMATTER = function (options) {
  var now = moment.utc();
  return util.format('%s %s %s', now.format(), options.level[0].toUpperCase(), options.message || '');
};

var SlackTransport = winston.transports.SlackTransport = function (options) {
  this.name = 'SlackTransport';
  this.level = options.level || 'info';

  this.slackWebhookUrl = options.slackWebhookUrl;
  this.domain = options.domain;
  this.channel = options.channel;
  this.icon_emoji = options.icon_emoji;
  this.username = options.username;
  this.formatter = options.formatter;
};
util.inherits(SlackTransport, winston.Transport);
SlackTransport.prototype.log = function (level, message, meta, callback) {
  var formattedMessage = '```' + this.formatter({
    level: level,
    message: message,
    meta: meta
  }) + '```';

  var payload = {
    channel: this.channel,
    username: this.username,
    text: formattedMessage,
    icon_emoji: this.icon_emoji
  };

  return request.post({url: this.slackWebhookUrl, form: {payload: JSON.stringify(payload)}}, callback);
};

// Logger instances
// ----------------

var logger = new (winston.Logger)({
  levels: LEVEL,
  transports: [
    new (winston.transports.Console)({
      level: process.env.CONSOLE_LOG_LEVEL || 'info',
      formatter: FORMATTER
    }),
    new (winstonDailyRotateFileTransport)({
      filename: './logs/app.log',
      level: process.env.FILE_LOG_LEVEL || 'verbose',
      json: false,
      formatter: FORMATTER
    }),
    new (SlackTransport)({
      domain: 'wiruh',
      slackWebhookUrl: process.env.SLACK_WEBHOOK_URL || '',
      channel: '#log-error',
      username: 'winston',
      level: 'error',
      icon_emoji: ':volcano:',
      formatter: FORMATTER
    })
  ]
});

// Create a logger with require('./path/to/this/file')(__filename)
// We will append __filename (the file path) of that module in the beginning of our log
// -----------------
module.exports = function (filepath) {
  return createLoggerAppendage(filepath);
};

function createLoggerAppendage(filepath) {
  var instance = {};

  let relFilePath = '=' + filepath.split(global.appRoot)[1];

  _.keys(LEVEL).forEach(function createLogPipe(key) {
    instance[key] = function () {
      // Interpolate string on the spot
      logger.log(key, relFilePath + ' - ' + util.format.apply(util, Array.prototype.slice.call(arguments)));
    };
  });
  return instance;
}
