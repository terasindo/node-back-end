'use strict';

module.exports = {
  FILE: {
    GRAPH: '../data/graph.in',
    TRAVEL_PATH: '../data/path.aux',
    WAITING_TIME_AUTOCOMPLETE: '../data/autocomplete.aux'
  },
  FOLDER: {
    AGGREGATED: '../data/aggregated'
  },
  EXTENSION: {
    WAITING_TIME: '.wt',
    TRAVEL_TIME: '.tt'
  },
  GRANULARITY: {
    WAITING_TIME: 5,
    TRAVEL_TIME: 5
  }
};
