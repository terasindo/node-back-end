'use strict';

var _ = require('lodash');
var redis = require('redis');
var bpromise = require('bluebird');

var logger = require('./logger')(__filename);
var redis = require('./redis');
var redisClient;

var self = module.exports;

module.exports.init = function () {
  if (process.env.ENABLE_REDIS === 'true') {
    logger.info('Setting up cache');
    redisClient = redis.getClient();
  } else {
    logger.info('Not using cache');
  }
};

module.exports.get = function (key, getFunction, options) {
  options = options || {};

  var ttl = options.ttl || -1;
  var raw = options.raw || false;
  var negativeCaching = options.negativeCaching || false;

  if (!redisClient || !redisClient.ready) {
    return bpromise.resolve()
      .then(getFunction);
  }

  return redisClient.getAsync(key)
    .catch(function onFailGet(err) {
      logger.error('Failed to get from redis, key = ' + key + ' error = ' + err);
      return null;
    })
    .then(function (result) {
      if (result !== null) {
        logger.verbose('Using cache!');
        if (raw) {
          return result;
        }
        return JSON.parse(result);
      }

      // Compute
      return bpromise.resolve()
        .then(getFunction)
        .catch(function onFailQuery(err) {
          logger.error('Failed to query before saving to cache, key = ' + key + ' error = ' + err);
        })
        .then(function (queriedResult) {
          if ((!negativeCaching && queriedResult) || negativeCaching) {
            // Set (spawn separated async chain)
            self.set(key, queriedResult, {
              ttl: ttl,
              raw: raw
            });
          }

          return queriedResult;
        });
    });
};

module.exports.set = function (key, value, options) {
  options = options || {};

  var ttl = options.ttl || -1;
  var raw = options.raw || false;

  var toCache;
  if (raw) {
    toCache = value;
  } else {
    toCache = JSON.stringify(value);
  }

  return redisClient.setAsync(key, toCache)
    .catch(function onFailSet(err) {
      logger.error('Failed to set to redis, key = ' + key + ' value = ' + toCache + ' error = ' + err);
      return null;
    })
    .then(function setExpiration() {
      if (ttl === -1) {
        return;
      }

      return redisClient.expireAsync(key, ttl)
        .catch(function onFailExpiration(err) {
          logger.error('Failed to expire to redis, key = ' + key + ' ttl = ' + ttl + ' error = ' + err);
          return null;
        });
    });
};

module.exports.mget = function (keyFuncObj, options) {
  options = options || {};

  var ttl = options.ttl || -1;
  var raw = options.raw || false;

  if (!redisClient || !redisClient.ready) {
    return bpromise.props(_.mapValues(keyFuncObj, function (f) {
      return f();
    }));
  }

  var keys = _.keys(keyFuncObj);
  return redisClient.mgetAsync(keys)
    .catch(function onFailGet(err) {
      logger.error('Failed to get from redis, key = ' + keys + ' error = ' + err);
      return null;
    })
    .then(function (results) {
      var resObj = {};
      var cacheMiss = {};

      keys.forEach(function (key, idx) {
        if (results[idx] !== null) {
          logger.verbose('Using cache!');
          if (raw) {
            resObj[key] = results[idx];
          } else {
            console.log(results[idx]);
            resObj[key] = JSON.parse(results[idx]);
          }
        } else {
          // Cache miss
          cacheMiss[key] = keyFuncObj[key]();
        }
      });

      // Compute cache miss
      return bpromise.props(cacheMiss)
        .catch(function onFailQuery(err) {
          logger.error('Failed to query before saving to cache, key = ' + _.keys(cacheMiss) + ' error = ' + err);
        })
        .then(function (cacheMissProped) {
          _.keys(cacheMissProped).forEach(function (key) {
            var value = cacheMissProped[key];

            // Set (spawn separated async chain)
            self.set(key, value, {
              ttl: ttl,
              raw: raw
            });
          });

          // Extend result
          _.extend(resObj, cacheMissProped);
          return resObj;
        });
    });
};
