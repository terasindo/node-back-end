'use strict';

var _ = require('lodash');
var redis = require('redis');
var bpromise = require('bluebird');

var logger = require('./logger')(__filename);

bpromise.promisifyAll(redis.RedisClient.prototype);
bpromise.promisifyAll(redis.Multi.prototype);

var client;

module.exports.getClient = function () {
  if (!_.isUndefined(client)) {
    return client;
  }

  client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST, {
    auth_pass: process.env.REDIS_PASSWORD,
    retry_strategy: function (options) {
      if (options.error.code === 'ECONNREFUSED') {
          // End reconnecting on a specific error and flush all commands with a individual error
          return new Error('The server refused the connection');
      }
      if (options.total_retry_time > 1000 * 60 * 60) {
          // End reconnecting after a specific timeout and flush all commands with a individual error
          return new Error('Retry time exhausted');
      }
      if (options.times_connected > 10) {
          // End reconnecting with built in error
          return undefined;
      }
      // reconnect after
      return Math.max(options.attempt * 100, 3000);
    }
  });

  client.on('error', function (err) {
    logger.error('Redis error %s', err.stack || err);
  });

  return client;
};
