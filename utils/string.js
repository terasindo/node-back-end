'use strict';

/**
 * Convert your string to be safely put as URI
 */
module.exports.slugify = function (str) {
  var cleanStr = String(str)
    .replace(/[^-0-9a-zA-Z ]/g, ' ')
    .replace(/\s+/g, ' ')
    .replace(/^ | $/g, '')
    .replace(/ /g, '-')
    .replace(/-+/g, '-')
    .replace(/([a-z])([A-Z])/g, '$1-$2')
    .replace(/(\d)([a-zA-Z])/g, '$1-$2')
    .toLowerCase();

  return cleanStr;
};
