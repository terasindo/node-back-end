'use strict';

var _ = require('lodash');

var mathUtil = require('./math');

module.exports.removeOutlier = function (arr, options) {
  options = options || {};

  var leftTail = options.leftTail || 0.025;
  var rightTail = options.rightTail || 0.025;

  var lowerBound = leftTail * arr.length;
  var upperBound = (1 - rightTail) * arr.length;

  var arrSorted = _.sortBy(arr);

  return arrSorted.slice(lowerBound, upperBound + 1);
};

module.exports.clampToOutlierBound = function (arr, options) {
  options = options || {};

  var leftTail = options.leftTail || 0.025;
  var rightTail = options.rightTail || 0.025;

  var arrSorted = _.sortBy(arr);
  var lowerBound = leftTail * arr.length;
  var upperBound = (1 - rightTail) * arr.length;

  var minValue = arrSorted[Math.floor(lowerBound)];
  var maxValue = arrSorted[Math.floor(upperBound)];

  return _.map(arr, mathUtil.clamp.bind(mathUtil, minValue, maxValue));
};

module.exports.getMedian = function (arr) {
  if (arr.length === 0) {
    return undefined;
  }

  if (arr.length === 1) {
    return arr[0];
  }

  return arr[Math.ceil(arr.length / 2)];
};