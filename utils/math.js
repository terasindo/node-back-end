'use strict';

var _ = require('lodash');
var self = module.exports;

module.exports.EPS = 1e-14;
module.exports.DIST_TO_M = 109556.69756687246;

module.exports.ORIGIN = {
  latitude: 0,
  longitude: 0
};

module.exports.eq = function (a, b, tolerance) {
  return Math.abs(a - b) < tolerance;
};

module.exports.round = function (value, accuracy) {
  var tens = Math.pow(10, accuracy);
  return Math.round(value * tens) / tens;
};

module.exports.clamp = function (a, b, x) {
  return Math.max(a, Math.min(x, b));
};

module.exports.distanceSq = function (a, b) {
  var dLat = a.latitude - b.latitude;
  var dLon = a.longitude - b.longitude;

  return dLat*dLat + dLon*dLon;
};

module.exports.distance = function (a, b) {
  return Math.sqrt(self.distanceSq(a, b));
};

module.exports.dot = function (a, b) {
  return a.latitude*b.latitude + a.longitude*b.longitude;
};

module.exports.cross = function (a, b) {
  return a.latitude*b.longitude - a.longitude*b.latitude;
};

module.exports.subtract = function (a, b) {
  return {
    latitude: a.latitude - b.latitude,
    longitude: a.longitude - b.longitude
  };
};

module.exports.roundAllNumber = function (item, decimalPlace) {
  if (_.isNumber(item)) {
    return self.round(item, decimalPlace);
  }

  if (_.isDate(item) || _.isString(item) || _.isBoolean(item) || _.isEmpty(item)) {
    return item;
  }

  if (_.isArray(item)) {
    return _.map(item, function reccursive(val) {
      return self.roundAllNumber(val, decimalPlace);
    });
  }

  if (_.isObject(item)) {
    return _.mapValues(item, function reccursive(val) {
      return self.roundAllNumber(val, decimalPlace);
    });
  }

  throw new Error('Unknown data type in roundAllNumber');
};

/**
 * Computes the length of given vector (norm)
 * @return {number}
 */
module.exports.norm = function (vector) {
  return self.distance(vector, self.ORIGIN);
};

/**
 * Computes the length of given vector (norm), and square it
 * @return {number}
 */

module.exports.normSq = function (vector) {
  return self.distanceSq(vector, self.ORIGIN);
};

/**
 * Returns distance from a point to a line. Remember that line is infinite.
 * @return {number}
 */
module.exports.pointToLineDistance = function (point, lineEndA, lineEndB) {
  var a = self.subtract(lineEndB, lineEndA);
  var b = self.subtract(point, lineEndA);

  return Math.abs(self.cross(a, b)) / self.norm(a);
};

module.exports.pointToPolylineDistance = function (point, polyline) {
  var distance = self.pointToLineSegmentDistance(point, polyline[0], polyline[1]);
  for (var i = 2; i < polyline.length; i++) {
    distance = Math.min(distance, self.pointToLineSegmentDistance(point, polyline[i-1], polyline[i]));
  }
  return distance;
};

module.exports.pointToPolylineDistanceLessThan = function (point, polyline, value) {
  var distance = self.pointToLineSegmentDistance(point, polyline[0], polyline[1]);
  for (var i = 2; i < polyline.length; i++) {
    distance = Math.min(distance, self.pointToLineSegmentDistance(point, polyline[i-1], polyline[i]));

    if (distance < value) {
      return true;
    }
  }
  return (distance < value);
};

/**
 * Point P formed shadow with line (A, B) if the projection of AP to AB is totally inside AB
 *
 * P to line (A, B) forms shadow (symbolized with '*'):
 *    P
 *   /
 *  /
 * A--*-----B
 *
 * P to line (A, B) doesn't form shadow:
 *    P
 *     \
 *      \
 *       A--------B
 *
 * @return {boolean}
 */
module.exports.isPointToLineSegmentFormedShadow = function (point, segmentEndA, segmentEndB) {
  var AB = self.subtract(segmentEndB, segmentEndA);
  var scalarProjection = self.scalarProjection(point, segmentEndA, segmentEndB);
  return (0 <= scalarProjection) && (scalarProjection <= self.norm(AB));
};

/**
 * Returns the shortest distance required for the point to travel and touch the line segment.
 * @return {number}
 */
module.exports.pointToLineSegmentDistance = function (point, segmentEndA, segmentEndB) {
  if (self.isPointToLineSegmentFormedShadow(point, segmentEndA, segmentEndB)) {
    return self.pointToLineDistance(point, segmentEndA, segmentEndB);
  } else {
    return Math.min(self.distance(point, segmentEndA),
                    self.distance(point, segmentEndB));
  }
};

/**
 * Given point P and line segment(A, B).
 * |AP|*cos(theta), where theta is angle formed by AB to AP.
 *
 * In these examples, |AB| = 5:
 *    P
 *   /
 *  /           The result is 3
 * A==>--B
 *
 *    P
 *     \        The result is -3
 *      \
 *    <==A-----B
 *
 *         P
 *       _/
 *     _/
 *   _/
 *  /           The result is 8
 * A====B==>
 *
 * @return {number}
 */
module.exports.scalarProjection = function (point, segmentEndA, segmentEndB) {
  var AB = self.subtract(segmentEndB, segmentEndA);
  var AP = self.subtract(point, segmentEndA);

  return self.dot(AB, AP) / self.norm(AB);
};

module.exports.turnAngle = function (pointA, pointB, pointC) {
  var AB = self.subtract(pointB, pointA);
  var BC = self.subtract(pointC, pointB);

  var angle = Math.acos(self.dot(AB, BC) / (self.norm(AB) * self.norm(BC)));
  return 180 - (angle * 180 / Math.PI);
};

/**
 * Given polyline and basePolyline.
 *
 * Imagine if basePolyline is a road.
 * You are walking in that road from a position to another position, with each step recorded and later becomes polyline.
 * You may start and end at any point of that road.
 *
 * For each step you make, return the distance from that point to the road's starting point which make the most sense.
 *
 * Algorithm:
 * For each point P in polyline, find one segment AB in basePolyline which is closest to it.
 * Now computes the length from basePolyline's first segment to segment before AB, then adds it with scalarProjection(P, A,B)
 *
 * To speed it up, process P in increasing fashion + always believe the closest segment is always located in/after last processed segment.
 *
 * Note:
 * This method is purely improvisation, there is no such things as polyline-polyline projection in real world.
 * Algorithm may change, as long as it makes sense.
 *
 * @return {number[]}
 */
module.exports.polylinePolylineProjection = function (polyline, basePolyline) {
  var result = new Array(polyline.length);

  var j = 0; // Current active segment is (basePolyline[j], basePolyline[j+1])

  var currentSegmentLength = self.distance(basePolyline[j], basePolyline[j+1]);
  var distanceFullyCovered = 0;

  // Process polyline in increasing fashion
  for (var i = 0; i < polyline.length; i++) {
    var point = polyline[i];

    // Sorry for being ugly, but this is required for performance
    if (j+2 < basePolyline.length) {
      var currentDistance = self.pointToLineSegmentDistance(point, basePolyline[j], basePolyline[j+1]);
      var nextDistance = self.pointToLineSegmentDistance(point, basePolyline[j+1], basePolyline[j+2]);

      // While current active segment is worse candidate for point's closest segment than the next one, move forward
      while ((j+2 < basePolyline.length) && (currentDistance + self.EPS > nextDistance)) {
        distanceFullyCovered += self.distance(basePolyline[j], basePolyline[j+1]);
        j++;

        currentSegmentLength = self.distance(basePolyline[j], basePolyline[j+1]);

        currentDistance = nextDistance;
        if (j+2 < basePolyline.length) {
          nextDistance = self.pointToLineSegmentDistance(point, basePolyline[j+1], basePolyline[j+2]);
        }
      }
    }

    var distancePartiallyCovered = self.clamp(
      0,
      self.scalarProjection(point, basePolyline[j], basePolyline[j+1]),
      currentSegmentLength
    );

    // Now return the distance covered from basePolyline[0] to this point
    result[i] = distanceFullyCovered + distancePartiallyCovered;
  }

  return result;
};

