'use strict';

let _ = require('lodash');
let fs = require('fs-extra');
let path = require('path');
let BPromise = require('bluebird');

let cbb = require('../cbb/build/Release/cbb');
let logger = require('../utils/logger')(__filename);
let constants = require('../utils/constants');
let graph = require('./graph');

module.exports.init = function (date) {
  return BPromise.resolve();
};

module.exports.get = function (req, res, next) {
  let fromName = graph.getBusStopNameBySlug(req.params.from);
  let toName = graph.getBusStopNameBySlug(req.params.destination);
  let minuteOfDay = Number(req.params.minuteOfDay);

  logger.debug('get travel time %s %s %s', fromName, toName, minuteOfDay);

  return res.json(cbb.getTravelTime(fromName, toName, minuteOfDay));
}

module.exports.getAllEdge = function (req, res, next) {
  let corridorName = graph.getCorridorNameBySlug(req.params.corridor);

  return res.json(cbb.getEdgesInCorridorTravelTime(corridorName));
}