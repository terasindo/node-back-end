'use strict';

let _ = require('lodash');
let fs = require('fs-extra');
let path = require('path');
let BPromise = require('bluebird');

let cbb = require('../cbb/build/Release/cbb');
let logger = require('../utils/logger')(__filename);
let constants = require('../utils/constants');
let graph = require('./graph');

module.exports.init = function (date) {
  return BPromise.resolve();
};

module.exports.get = function (req, res, next) {
  let corridorName = graph.getCorridorNameBySlug(req.params.corridor);
  let fromName = graph.getBusStopNameBySlug(req.params.from);
  let destinationName = graph.getBusStopNameBySlug(req.params.destination);
  let minuteOfDay = Number(req.params.minuteOfDay);

  logger.debug('get waiting time %s %s %s %s', corridorName, fromName, destinationName, minuteOfDay);

  return res.json(cbb.getWaitingTime(corridorName, fromName, destinationName, minuteOfDay));
}

module.exports.getSingleToCommonBusStops = function (req, res, next) {
  let corridorName = graph.getCorridorNameBySlug(req.params.corridor);
  let fromName = graph.getBusStopNameBySlug(req.params.from);

  return res.json(cbb.getSingleToCommonBusStopsWaitingTime(corridorName, fromName));
}