'use strict';

let _ = require('lodash');
let fs = require('fs-extra');
let path = require('path');
let BPromise = require('bluebird');

let cbb = require('../cbb/build/Release/cbb');
let logger = require('../utils/logger')(__filename);
let graph = require('./graph');

module.exports.init = function () {
  return BPromise.resolve();
};

module.exports.getTravelPath = function (req, res) {
  let fromName = graph.getBusStopNameBySlug(req.params.from);
  let toName = graph.getBusStopNameBySlug(req.params.to);
  let minuteOfDay = Number(req.params.minuteOfDay);

  return res.json(cbb.getTravelPath(fromName, toName, minuteOfDay));
};