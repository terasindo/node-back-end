'use strict';

var _ = require('lodash');
let fs = require('fs-extra');
let path = require('path');
let stringUtil = require('../utils/string');
let constants = require('../utils/constants');
let logger = require('../utils/logger')(__filename);

let busStops = [];
let corridors = [];
let busStopBySlug = {};
let corridorBySlug = {};
let busStopByName = {};
let corridorByName = {};
let busStopSlugToName = {};
let corridorSlugToName = {};

module.exports.init = function () {
  let graphFile = path.join(global.appRoot, constants.FILE.GRAPH);
  logger.info('Init graph module using %s', graphFile);
  return fs.statAsync(graphFile)
    .then(function () {
      return fs.readFileAsync(graphFile, 'utf-8');
    })
    .then(function (result) {
      let lines = result.split('\n');

      let p = 0;
      let nBusStops = Number(lines[p++]);
      for (let i = 0; i < nBusStops; i++) {
        let name = lines[p++];

        let tokens = lines[p++].split(' ');
        let nAlias = Number(tokens[0]);
        let latitude = Number(tokens[1]);
        let longitude = Number(tokens[2]);
        let isTerminal = Number(tokens[3]);
        let isCommon = Number(tokens[4]);
        let slug = tokens[5];

        if (slug != stringUtil.slugify(name)) {
          throw new Error("Invalid bus stop slug detected: " + name + " => " + slug);
        }

        let aliases = [];
        for (let j = 0; j < nAlias; j++) {
          aliases.push(lines[p++]);
        }

        busStops.push({
          index: i,
          slug: slug,
          name: name,
          aliases: aliases,
          isTerminal: isTerminal == 1,
          isCommon: isCommon == 1,
          longitude: longitude,
          latitude: latitude,
          bridges: []
        });
      }

      let nAllBridge = Number(lines[p++]);
      for (let i = 0; i < nAllBridge; i++) {
        let tokens = lines[p++].split(' ');
        busStops[tokens[0]].bridges.push(busStops[tokens[1]]);
      }

      let nCorridor = Number(lines[p++]);
      for (let i = 0; i < nCorridor; i++) {
        let name = lines[p++];
        let fullName = lines[p++];

        let tokens = lines[p++].split(' ');

        // // I leave this comment on purpose for clarity
        // let nPointPolylineFromStart = Number(tokens[0]);
        // let nPointPolylineFromEnd = Number(tokens[1]);
        let startBusStopId = Number(tokens[2]);
        let endBusStopId = Number(tokens[3]);
        let slug = tokens[4];

        if (slug != stringUtil.slugify(name)) {
          throw new Error("Invalid corridor slug detected: " + name + " => " + slug);
        }

        let polylineFromStart = _.chain(lines[p++].split(' '))
          .chunk(2)
          .map(function (arr) {
            return {
              latitude: arr[0],
              longitude: arr[1]
            };
          })
          .value();
        let polylineFromEnd = _.chain(lines[p++].split(' '))
          .chunk(2)
          .map(function (arr) {
            return {
              latitude: arr[0],
              longitude: arr[1]
            };
          })
          .value();

        corridors.push({
          index: i,
          slug: slug,
          name: name,
          fullName: fullName,
          startBusStop: busStops[startBusStopId],
          endBusStop: busStops[endBusStopId],
          polylineFromStart: polylineFromStart,
          polylineFromEnd: polylineFromEnd
        });
      }

      busStopBySlug = _.keyBy(busStops, 'slug');
      corridorBySlug = _.keyBy(corridors, 'slug');

      busStopByName = _.keyBy(busStops, 'name');
      corridorByName = _.keyBy(corridors, 'name');

      busStopSlugToName = _.mapValues(_.keyBy(busStops, 'slug'), 'name');
      corridorSlugToName = _.mapValues(_.keyBy(corridors, 'slug'), 'name');
    });
};

module.exports.getBusStopBySlug = function (slug) {
  return busStopBySlug[slug];
};

module.exports.getCorridorBySlug = function (slug) {
  return corridorBySlug[slug];
};

module.exports.getBusStopByName = function (name) {
  return busStopByName[name];
};

module.exports.getCorridorByName = function (name) {
  return corridorByName[name];
};

module.exports.getBusStopNameBySlug = function (slug) {
  return busStopSlugToName[slug];
};

module.exports.getCorridorNameBySlug = function (slug) {
  return corridorSlugToName[slug];
};
