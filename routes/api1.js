'use strict';

var express = require('express');
var router = require('route-label')(express());

var travelPath = require('../modules/travelPath');
var travelTime = require('../modules/travelTime');
var waitingTime = require('../modules/waitingTime');

router.get('travelPath', '/travel-path/:from/:to/:minuteOfDay', travelPath.getTravelPath.bind(travelPath));

router.get('waitingTime', '/waiting-time/:corridor/:from/:destination/:minuteOfDay', waitingTime.get);
router.get('travelTime', '/travel-time/:from/:destination/:minuteOfDay', travelTime.get);

router.get('waitingTimeSingleToImportantBusStops', '/waiting-time/:corridor/:from', waitingTime.getSingleToCommonBusStops);
router.get('TravelTimeEdgesInCorridor', '/travel-time/:corridor', travelTime.getAllEdge);

module.exports = router;
