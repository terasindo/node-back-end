'use strict';

var express = require('express');
var router = require('route-label')(express());

router.get('index', '/', function(req, res) {
  res.send('Ready!');
});

// Default version
router.use('api', '/api', require('./api1'));

// Version specific for future
router.use('api.v1', '/api/v1', require('./api1'));

// Build route table so we can generate URL based on name
router.buildRouteTable();

module.exports = router;
