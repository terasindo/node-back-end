#include <node.h>
#include <v8.h>
#include <uv.h>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

#include "config.hpp"
#include "graph.hpp"
#include "waitingTime.hpp"
#include "travelTime.hpp"
#include "travelPath.hpp"
#include "waitingTimeAutocomplete.hpp"

using namespace v8;

// Global objects
Config *gConfig = NULL;
Graph *gGraph = NULL;
WaitingTime* gWaitingTime = NULL;
TravelTime* gTravelTime = NULL;
TravelPath* gTravelPath = NULL;
WaitingTimeAutocomplete* gWaitingTimeAutocomplete = NULL;

inline string getString(const Handle<Value> &value) {
  return std::string(*v8::String::Utf8Value(value));
}

void init(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Handle<Object> argObj = Handle<Object>::Cast(args[0]);

  string graphFile = getString(argObj->Get(String::NewFromUtf8(isolate, "GRAPH_FILE")));
  string waitingTimeFile = getString(argObj->Get(String::NewFromUtf8(isolate, "WAITING_TIME_FILE")));
  string travelTimeFile = getString(argObj->Get(String::NewFromUtf8(isolate, "TRAVEL_TIME_FILE")));
  string travelPathFile = getString(argObj->Get(String::NewFromUtf8(isolate, "TRAVEL_PATH_FILE")));
  string waitingTimeAutocompleteFile = getString(argObj->Get(String::NewFromUtf8(isolate, "WAITING_TIME_AUTOCOMPLETE_FILE")));
  string env = getString(argObj->Get(String::NewFromUtf8(isolate, "NODE_ENV")));

  gConfig = new Config(env);
  gGraph = new Graph(graphFile);
  gWaitingTimeAutocomplete = new WaitingTimeAutocomplete(gConfig, gGraph, waitingTimeAutocompleteFile);
  gWaitingTime = new WaitingTime(gConfig, gGraph, gWaitingTimeAutocomplete, waitingTimeFile);
  gTravelTime = new TravelTime(gConfig, gGraph, travelTimeFile);
  gTravelPath = new TravelPath(gConfig, gGraph, gWaitingTime, gTravelTime, travelPathFile);

  bool initSuccess =
      !gGraph->isFileCorrupted() &&
      !gWaitingTime->isFileCorrupted() &&
      !gTravelTime->isFileCorrupted() &&
      !gTravelPath->isFileCorrupted() &&
      !gWaitingTimeAutocomplete->isFileCorrupted();

  args.GetReturnValue().Set(initSuccess);
}

void getWaitingTime(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  string corridorName = getString(Handle<Value>::Cast(args[0]));
  string fromName = getString(Handle<Value>::Cast(args[1]));
  string destinationName = getString(Handle<Value>::Cast(args[2]));
  int minuteOfDay = Handle<Value>::Cast(args[3])->NumberValue();

  int corridor = gGraph->getCorridorIdx(corridorName);
  int from = gGraph->getBusStopIdx(fromName);
  int destination = gGraph->getBusStopIdx(destinationName);

  args.GetReturnValue().Set(gWaitingTime->getMeasurement(isolate, corridor, from, destination, minuteOfDay));
}

void getTravelTime(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  string fromName = getString(Handle<Value>::Cast(args[0]));
  string toName = getString(Handle<Value>::Cast(args[1]));
  int minuteOfDay = Handle<Value>::Cast(args[2])->NumberValue();

  int from = gGraph->getBusStopIdx(fromName);
  int to = gGraph->getBusStopIdx(toName);

  args.GetReturnValue().Set(gTravelTime->getMeasurement(isolate, from, to, minuteOfDay));
}

void getTravelPath(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  string fromName = getString(Handle<Value>::Cast(args[0]));
  string toName = getString(Handle<Value>::Cast(args[1]));
  int minuteOfDay = Handle<Value>::Cast(args[2])->NumberValue();

  int from = gGraph->getBusStopIdx(fromName);
  int to = gGraph->getBusStopIdx(toName);

  args.GetReturnValue().Set(gTravelPath->get(isolate, from, to, minuteOfDay));
}

void getSingleToCommonBusStopsWaitingTime(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  string corridorName = getString(Handle<Value>::Cast(args[0]));
  string fromName = getString(Handle<Value>::Cast(args[1]));

  int corridor = gGraph->getCorridorIdx(corridorName);
  int from = gGraph->getBusStopIdx(fromName);

  args.GetReturnValue().Set(gWaitingTime->getSingleToCommonBusStops(isolate, corridor, from));
}

void getEdgesInCorridorTravelTime(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  string corridorName = getString(Handle<Value>::Cast(args[0]));

  int corridor = gGraph->getCorridorIdx(corridorName);

  args.GetReturnValue().Set(gTravelTime->getAllEdge(isolate, corridor));
}

void initModule(Handle <Object> exports, Handle<Object> module) {
  NODE_SET_METHOD(exports, "init", init);

  NODE_SET_METHOD(exports, "getTravelPath", getTravelPath);

  NODE_SET_METHOD(exports, "getWaitingTime", getWaitingTime);
  NODE_SET_METHOD(exports, "getTravelTime", getTravelTime);

  // For MFA
  NODE_SET_METHOD(exports, "getSingleToCommonBusStopsWaitingTime", getSingleToCommonBusStopsWaitingTime);
  NODE_SET_METHOD(exports, "getEdgesInCorridorTravelTime", getEdgesInCorridorTravelTime);
}

NODE_MODULE(cbb, initModule)
