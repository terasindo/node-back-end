// Diabolic magic aheads...
#pragma once

#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <set>
#include <vector>

#include "config.hpp"
#include "graph.hpp"
#include "waitingTimeAutocomplete.hpp"

#include <node.h>
#include <v8.h>
#include <uv.h>

using v8::Local;
using v8::Object;
using v8::Isolate;
using v8::Number;
using v8::Integer;
using v8::Array;
using v8::String;

using namespace std;

class WaitingTime {
private:
  class WaitingTimeBundle {
  public:
    int gCorridorIdx;
    int gDestinationIdx;
    int gFromIdx;
    int minuteOfDay;
    vector<vector<char>> values;
  };

  const int NUMBERS_PER_ENTRY = 6;
  const int INDEX_MEAN = 0;
  const int INDEX_MEDIAN = 1;
  const int INDEX_P75 = 2;
  const int INDEX_P80 = 3;
  const int INDEX_P85 = 4;
  const int INDEX_P90 = 5;

  const int API_MINUTE_GRANULARITY = 30;
  const int MAX_WAITING_TIME = 90;

  // MEGA STRUCTURE: [dayIdx][minute][corridor][to][from] = [x, y, z]
  char ******data; // Using primitive structure for super speed
  int minuteGranularity;
  int defaultWindowDayIdx;

  // Mega structure supporting data
  int *windowDays;
  int *iWindowDays;
  int *corridor;
  int *iCorridor;
  int **destinationByCorridor;
  int **iDestinationByCorridor;
  int ***fromByCorridorAndDestination;
  int ***iFromByCorridorAndDestination;

  bool isCorrupted;
  Graph *mGraph;
  Config *mConfig;
  WaitingTimeAutocomplete *mWaitingTimeAutocomplete;

public:
  WaitingTime(Config* config, Graph* graph, WaitingTimeAutocomplete* waitingTimeAutocomplete, string filePath) {
    mGraph = graph;
    mConfig = config;
    mWaitingTimeAutocomplete = waitingTimeAutocomplete;

    FILE *file = fopen(filePath.c_str(), "r");

    if (!file) {
      isCorrupted = true;
      return;
    }

    isCorrupted = false;

    map<int,int> l2gBlusStopIdx;
    map<int,int> l2gCorridorIdx;

    char buff[128];

    int nBusStop;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nBusStop) != 1);
    for (int i = 0; i < nBusStop; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string name = buff;
      l2gBlusStopIdx[i] = graph->getBusStopIdx(name);
    }
    int nCorridor;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nCorridor) != 1);
    for (int i = 0; i < nCorridor; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string name = buff;
      l2gCorridorIdx[i] = graph->getCorridorIdx(name);
    }

    int nWindowDay;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nWindowDay) != 1);
    windowDays = new int[nWindowDay];
    for (int i = 0; i < nWindowDay; i++) {
      int day;
      isCorrupted = isCorrupted || (fscanf(file, "%d\n", &day) != 1);
      windowDays[i] = day;
    }

    // Set default window day to the longest
    defaultWindowDayIdx = nWindowDay - 1;

    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &minuteGranularity) != 1);

    // Temporary storage, will be destroyed automatically in the end of this constructor
    vector<WaitingTimeBundle> waitingTimeBundles;
    int nWaitingTime;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nWaitingTime) != 1);
    for (int i = 0; i < nWaitingTime; i++) {
      int lCorridorIdx;
      int lFromIdx;
      int lDestinationIdx;
      int minuteOfDay;
      isCorrupted = isCorrupted || (fscanf(file, "%d %d %d %d\n", &lCorridorIdx, &lFromIdx, &lDestinationIdx, &minuteOfDay) != 4);

      WaitingTimeBundle bundle;
      bundle.gCorridorIdx = l2gCorridorIdx[lCorridorIdx];
      bundle.gDestinationIdx = l2gBlusStopIdx[lDestinationIdx];
      bundle.gFromIdx = l2gBlusStopIdx[lFromIdx];
      bundle.minuteOfDay = minuteOfDay;

      for (int j = 0; j < nWindowDay; j++) {
        vector<char> numsInDay;
        for (int k = 0; k < NUMBERS_PER_ENTRY; k++) {
          int x;
          isCorrupted = isCorrupted || (fscanf(file, "%d", &x) != 1);

          // Clean
          x = min(max(0, x), MAX_WAITING_TIME);
          if (x == 0) {
            x = MAX_WAITING_TIME; // Data unavailable
          }

          numsInDay.push_back(x);

          if (k + 1 < NUMBERS_PER_ENTRY) {
            isCorrupted = isCorrupted || (fscanf(file, " ") != 0);
          } else {
            isCorrupted = isCorrupted || (fscanf(file, "\n") != 0);
          }
        }
        bundle.values.push_back(numsInDay);
      }

      waitingTimeBundles.push_back(bundle);
    }

    fclose(file);

    // Begin countings
    set<int> destinations[nCorridor];
    set<int> froms[nCorridor][nBusStop];

    for (int i = 0; i < (int)waitingTimeBundles.size(); i++) {
      int c = waitingTimeBundles[i].gCorridorIdx;
      int d = waitingTimeBundles[i].gDestinationIdx;
      int f = waitingTimeBundles[i].gFromIdx;

      destinations[c].insert(d);
      froms[c][d].insert(f);
    }

    // Construct structure
    int maxWindowDay = 0;
    for (int i = 0; i < nWindowDay; i++) {
      maxWindowDay = max(maxWindowDay, windowDays[i]);
    }
    iWindowDays = new int[maxWindowDay + 1];
    for (int i = 0; i <= maxWindowDay; i++) {
      iWindowDays[i] = -1;
    }
    for (int i = 0; i < nWindowDay; i++) {
      iWindowDays[windowDays[i]] = i;
    }

    corridor = new int[nCorridor];
    iCorridor = new int[nCorridor];
    for (int i = 0; i < nCorridor; i++) {
      // Yea these look stupid
      // But let's be consistent by providing all supporting data to avoid future headache by assumption
      corridor[i] = i;
      iCorridor[i] = i;
    }

    destinationByCorridor = new int*[nCorridor];
    iDestinationByCorridor = new int*[nCorridor];
    for (int i = 0; i < nCorridor; i++) {
      destinationByCorridor[i] = new int[destinations[i].size()];
      iDestinationByCorridor[i] = new int[nBusStop];
      int j = 0;
      for (int d : destinations[i]) {
        destinationByCorridor[i][j] = d;
        iDestinationByCorridor[i][d] = j;
        j++;
      }
    }

    fromByCorridorAndDestination = new int**[nCorridor];
    iFromByCorridorAndDestination = new int**[nCorridor];
    for (int i = 0; i < nCorridor; i++) {
      fromByCorridorAndDestination[i] = new int*[destinations[i].size()];
      iFromByCorridorAndDestination[i] = new int*[destinations[i].size()];

      for (int j = 0; j < (int)destinations[i].size(); j++) {
        int d = destinationByCorridor[i][j];
        fromByCorridorAndDestination[i][j] = new int[froms[i][d].size()];
        iFromByCorridorAndDestination[i][j] = new int[nBusStop];

        int k = 0;
        for (int f : froms[i][d]) {
          fromByCorridorAndDestination[i][j][k] = f;
          iFromByCorridorAndDestination[i][j][f] = k;
          k++;
        }
      }
    }

    // Construct mega structure
    data = new char*****[nWindowDay];
    for (int d = 0; d < nWindowDay; d++) {
      int sz = 24 * 60 / minuteGranularity;
      data[d] = new char****[sz];

      for (int t = 0; t < sz; t++) {
        data[d][t] = new char***[nCorridor];

        for (int corr = 0; corr < nCorridor; corr++) {
          data[d][t][corr] = new char**[destinations[corr].size()];

          for (int dest = 0; dest < (int)destinations[corr].size(); dest++) {
            int destIdx = destinationByCorridor[corr][dest];
            data[d][t][corr][dest] = new char*[froms[corr][destIdx].size()];
          }
        }
      }
    }

    // Fill mega structure
    for (int i = 0; i < (int)waitingTimeBundles.size(); i++) {
      int c = waitingTimeBundles[i].gCorridorIdx;
      int d = waitingTimeBundles[i].gDestinationIdx;
      int f = waitingTimeBundles[i].gFromIdx;
      int t = waitingTimeBundles[i].minuteOfDay / minuteGranularity;

      vector<vector<char>> &values = waitingTimeBundles[i].values;
      for (int day = 0; day < nWindowDay; day++) {
        int destIdx = iDestinationByCorridor[c][d];
        int fromIdx = iFromByCorridorAndDestination[c][destIdx][f];

        data[day][t][c][destIdx][fromIdx] = new char[6]{
            values[day][INDEX_MEAN],
            values[day][INDEX_MEDIAN],
            values[day][INDEX_P75],
            values[day][INDEX_P80],
            values[day][INDEX_P85],
            values[day][INDEX_P90],
        };
      }
    }
  };
  ~WaitingTime() {};

  bool isFileCorrupted() {
    return isCorrupted;
  }

  char* getDataAddress(int corridor, int from, int destination, int minuteOfDay) {
    int correctedDestination = mWaitingTimeAutocomplete->get(corridor, from, destination);

    int t = minuteOfDay / minuteGranularity;
    int destIdx = iDestinationByCorridor[corridor][correctedDestination];
    int fromIdx = iFromByCorridorAndDestination[corridor][destIdx][from];

    if (mConfig->isDevelopmentMode()) {
      printf(
          "Request for [%s] '%s' => '%s' (corrected: '%s')\n",
          mGraph->getCorridorName(corridor).c_str(),
          mGraph->getBusStopName(fromByCorridorAndDestination[corridor][destIdx][fromIdx]).c_str(),
          mGraph->getBusStopName(destination).c_str(),
          mGraph->getBusStopName(destinationByCorridor[corridor][destIdx]).c_str()
      );
    }

    return data[defaultWindowDayIdx][t][corridor][destIdx][fromIdx];
  }

  int getMedian(int corridor, int from, int destination, int minuteOfDay) {
    return getDataAddress(corridor, from, destination, minuteOfDay)[INDEX_MEDIAN];
  }

  int getMean(int corridor, int from, int destination, int minuteOfDay) {
    return getDataAddress(corridor, from, destination, minuteOfDay)[INDEX_MEAN];
  }

  int getP75(int corridor, int from, int destination, int minuteOfDay) {
    return getDataAddress(corridor, from, destination, minuteOfDay)[INDEX_P75];
  }

  int getP80(int corridor, int from, int destination, int minuteOfDay) {
    return getDataAddress(corridor, from, destination, minuteOfDay)[INDEX_P80];
  }

  int getP85(int corridor, int from, int destination, int minuteOfDay) {
    return getDataAddress(corridor, from, destination, minuteOfDay)[INDEX_P85];
  }

  int getP90(int corridor, int from, int destination, int minuteOfDay) {
    return getDataAddress(corridor, from, destination, minuteOfDay)[INDEX_P90];
  }

  Local<Object> getMeasurement(Isolate* &isolate, int corridor, int from, int destination, int minuteOfDay) {
    int mean = getMean(corridor, from, destination, minuteOfDay);
    int median = getMedian(corridor, from, destination, minuteOfDay);
    int p75 = getP75(corridor, from, destination, minuteOfDay);
    int p80 = getP80(corridor, from, destination, minuteOfDay);
    int p85 = getP85(corridor, from, destination, minuteOfDay);
    int p90 = getP90(corridor, from, destination, minuteOfDay);

    Local<Object> result = Object::New(isolate);

    result->Set(String::NewFromUtf8(isolate, "mean"), Number::New(isolate, mean));
    result->Set(String::NewFromUtf8(isolate, "median"), Number::New(isolate, median));
    result->Set(String::NewFromUtf8(isolate, "p75"), Number::New(isolate, p75));
    result->Set(String::NewFromUtf8(isolate, "p80"), Number::New(isolate, p80));
    result->Set(String::NewFromUtf8(isolate, "p85"), Number::New(isolate, p85));
    result->Set(String::NewFromUtf8(isolate, "p90"), Number::New(isolate, p90));

    return result;
  }

  /*
   * {
   *   minuteOfDays: [0, 10, 20, 30, ..., 1430],
   *   data: [
   *     {
   *       to: "Harmoni",
   *       mean: [5, 4, 5, ...],
   *       p90: [9, 9, 8, ...]
   *     }
   *   ]
   * }
   */
  Local<Object> getSingleToCommonBusStops(Isolate* &isolate, int corridor, int from) {
    Local<Object> result = Object::New(isolate);

    // Put minuteOfDays
    Local<Array> minuteOfDays = Array::New(isolate);
    for (int i = 0; i*API_MINUTE_GRANULARITY < 24*60; i++) {
      minuteOfDays->Set(i, Integer::New(isolate, i * API_MINUTE_GRANULARITY));
    }

    // Get all common bus stops
    const vector<int> &commonBusStops = mGraph->getAddedCommonBusStopIdxs(corridor, from);

    // Put data
    Local<Array> data = Array::New(isolate);
    for (int i = 0; i < (int)commonBusStops.size(); i++) {
      Local<Object> record = Object::New(isolate);

      Local<Array> means = Array::New(isolate);
      Local<Array> medians = Array::New(isolate);
      Local<Array> p75s = Array::New(isolate);
      Local<Array> p80s = Array::New(isolate);
      Local<Array> p85s = Array::New(isolate);
      Local<Array> p90s = Array::New(isolate);
      for (int j = 0; j*API_MINUTE_GRANULARITY < 24*60; j++) {
        char* item = getDataAddress(corridor, from, commonBusStops[i], j*API_MINUTE_GRANULARITY);

        if (from == commonBusStops[i]) {
          means->Set(j, Integer::New(isolate, 0));
          medians->Set(j, Integer::New(isolate, 0));
          p75s->Set(j, Integer::New(isolate, 0));
          p80s->Set(j, Integer::New(isolate, 0));
          p85s->Set(j, Integer::New(isolate, 0));
          p90s->Set(j, Integer::New(isolate, 0));
        } else {
          means->Set(j, Integer::New(isolate, item[INDEX_MEAN]));
          medians->Set(j, Integer::New(isolate, item[INDEX_MEDIAN]));
          p75s->Set(j, Integer::New(isolate, item[INDEX_P75]));
          p80s->Set(j, Integer::New(isolate, item[INDEX_P80]));
          p85s->Set(j, Integer::New(isolate, item[INDEX_P85]));
          p90s->Set(j, Integer::New(isolate, item[INDEX_P90]));
        }
      }

      record->Set(String::NewFromUtf8(isolate, "to"), String::NewFromUtf8(isolate, mGraph->getBusStopName(commonBusStops[i]).c_str()));
      record->Set(String::NewFromUtf8(isolate, "mean"), means);
      record->Set(String::NewFromUtf8(isolate, "median"), medians);
      record->Set(String::NewFromUtf8(isolate, "p75"), p75s);
      record->Set(String::NewFromUtf8(isolate, "p80"), p80s);
      record->Set(String::NewFromUtf8(isolate, "p85"), p85s);
      record->Set(String::NewFromUtf8(isolate, "p90"), p90s);

      data->Set(i, record);
    }

    result->Set(String::NewFromUtf8(isolate, "minuteOfDays"), minuteOfDays);
    result->Set(String::NewFromUtf8(isolate, "data"), data);
    return result;
  }
};
