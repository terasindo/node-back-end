#pragma once

#include <vector>
#include <string>

#include "busStop.hpp"

using std::string;
using std::vector;

class BusStop;
class Corridor {
public:
  int index;
  string name;
  string slug;
  string fullName;
  BusStop* startBusStop;
  BusStop* endBusStop;

  Corridor(
      int _index,
      string _name,
      string _slug,
      string _fullName,
      BusStop* _startBusStop,
      BusStop* _endBusStop
    ) :
      index(_index),
      name(_name),
      slug(_slug),
      fullName(_fullName),
      startBusStop(_startBusStop),
      endBusStop(_endBusStop)
    {
  }

  ~Corridor () {
  }
};
