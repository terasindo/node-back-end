#pragma once

#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <set>
#include <vector>

#include "config.hpp"
#include "graph.hpp"
#include "waitingTime.hpp"
#include "travelTime.hpp"
#include "pathNode.hpp"

#include <node.h>
#include <v8.h>
#include <uv.h>

using v8::Local;
using v8::Object;
using v8::Isolate;
using v8::Number;
using v8::Integer;
using v8::String;
using v8::Array;
using namespace std;

class TravelPath {
private:
  static constexpr double DIST_TO_M = 109556.69756687246;
  static constexpr double WALKING_SPEED_M_PER_MINUTE = 66;
  static constexpr double WALK_TIME_CONST = DIST_TO_M / WALKING_SPEED_M_PER_MINUTE;

  bool isCorrupted;
  Graph *mGraph;
  Config *mConfig;
  WaitingTime *mWaitingTime;
  TravelTime *mTravelTime;

  vector<vector<PathNode>> **paths;

public:
  TravelPath(Config* config, Graph* graph, WaitingTime* waitingTime, TravelTime* travelTime, string filePath) {
    mGraph = graph;
    mConfig = config;
    mWaitingTime = waitingTime;
    mTravelTime = travelTime;

    FILE *file = fopen(filePath.c_str(), "r");

    if (!file) {
      isCorrupted = true;
      return;
    }

    isCorrupted = false;

    map<int,int> l2gBlusStopIdx;
    map<int,int> l2gCorridorIdx;

    char buff[128];

    int nBusStop;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nBusStop) != 1);
    for (int i = 0; i < nBusStop; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string name = buff;
      l2gBlusStopIdx[i] = graph->getBusStopIdx(name);
    }
    int nCorridor;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nCorridor) != 1);
    for (int i = 0; i < nCorridor; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string name = buff;
      l2gCorridorIdx[i] = graph->getCorridorIdx(name);
    }

    paths = new vector<vector<PathNode>>*[nBusStop];
    for (int i = 0; i < nBusStop; i++) {
      paths[i] = new vector<vector<PathNode>>[nBusStop];
    }

    int nTravelPath;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nTravelPath) != 1);
    for (int i = 0; i < nTravelPath; i++) {
      int lFromIdx;
      int lToIdx;
      int nPath;
      isCorrupted = isCorrupted || (fscanf(file, "%d %d %d\n", &lFromIdx, &lToIdx, &nPath) != 3);

      int gFromIdx = l2gBlusStopIdx[lFromIdx];
      int gToIdx = l2gBlusStopIdx[lToIdx];

      vector<vector<PathNode>> &bucket = paths[gFromIdx][gToIdx];
      for (int j = 0; j < nPath; j++) {
        int nPathNode;
        float rank;
        isCorrupted = isCorrupted || (fscanf(file, "%d %f\n", &nPathNode, &rank) != 2);

        vector<PathNode> path;
        for (int k = 0; k < nPathNode; k++) {
          int action;
          int lBusStopIdx;
          isCorrupted = isCorrupted || (fscanf(file, "%d %d\n", &action, &lBusStopIdx) != 2);

          int gBusStopIdx = l2gBlusStopIdx[lBusStopIdx];
          vector<int> activeCorridors;
          vector<int> activeCorridorEnds;
          if (action == PathNode::ACTION_GET_ON) {
            int nActiveCorridor;
            isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nActiveCorridor) != 1);
            for (int p = 0; p < nActiveCorridor; p++) {
              int lCorridorIdx;
              int lEndIdx;
              isCorrupted = isCorrupted || (fscanf(file, "%d %d", &lCorridorIdx, &lEndIdx) != 2);

              activeCorridors.push_back(l2gCorridorIdx[lCorridorIdx]);
              activeCorridorEnds.push_back(l2gBlusStopIdx[lEndIdx]);
            }
          }

          path.push_back(PathNode(action, gBusStopIdx, activeCorridors, activeCorridorEnds));
        }

        bucket.push_back(path);
      }
    }

    fclose(file);
  };
  ~TravelPath() {};

  bool isFileCorrupted() {
    return isCorrupted;
  }

  Local<Array> get(Isolate* &isolate, int from, int to, int minuteOfDay) {
    const vector<vector<PathNode>> &ret = paths[from][to];

    // Decorate path with time
    Local<Array> retVal = Array::New(isolate);
    for (int i = 0; i < (int)ret.size(); i++) {
      Local<Object> pathObj = Object::New(isolate);
      Local<Array> pathNodesArr = Array::New(isolate);
      int currMinuteOfDay = minuteOfDay;
      int transitCount = 0;
      string summary = "";

      const vector<PathNode> &pathNodes = ret[i];
      for (int j = 0; j < (int)pathNodes.size(); j++) {
        const PathNode &pathNode = pathNodes[j];
        Local<Object> nodeObj = Object::New(isolate);

        // Move time
        int duration = 0;
        if (pathNode.action == PathNode::ACTION_GET_ON) {
          // Waiting time
          int getOffIdx = j;
          while (pathNodes[getOffIdx].action != PathNode::ACTION_GET_OFF) {
            getOffIdx++;
          }
          duration = mWaitingTime->getMedian(pathNode.activeCorridors[0], pathNode.busStop, pathNodes[getOffIdx].busStop, currMinuteOfDay);
        } else if (pathNode.action == PathNode::ACTION_IN_BUS) {
          // Travel time
          duration = mTravelTime->getMedian(pathNodes[j-1].busStop, pathNode.busStop, currMinuteOfDay);
        } else if (pathNode.action == PathNode::ACTION_CROSS_BRIDGE) {
          // Crossing bridge time
          duration = mGraph->getDistance(pathNodes[j-1].busStop, pathNode.busStop) * WALK_TIME_CONST;
        }

        // Transit?
        if ((pathNode.action == PathNode::ACTION_GET_ON) && (j > 1)) {
          transitCount++;
        }

        string corridorString = "";
        Local<Array> corridorArr = Array::New(isolate);
        for (int k = 0; k < (int)pathNode.activeCorridors.size(); k++) {

          Local<Array> pairArr = Array::New(isolate);
          pairArr->Set(0, String::NewFromUtf8(isolate, mGraph->getCorridorName(pathNode.activeCorridors[k]).c_str()));
          pairArr->Set(1, String::NewFromUtf8(isolate, mGraph->getBusStopName(pathNode.activeCorridorEnds[k]).c_str()));

          corridorArr->Set(k, pairArr);

          if (corridorString.length() > 0) {
            corridorString += "/";
          }
          corridorString += mGraph->getCorridorName(pathNode.activeCorridors[k]);
        }

        if (corridorString.length() > 0) {
          if (summary.length() > 0) {
            summary += " - ";
          }
          summary += corridorString;
        }

        nodeObj->Set(String::NewFromUtf8(isolate, "busStop"), String::NewFromUtf8(isolate, mGraph->getBusStopName(pathNode.busStop).c_str()));
        nodeObj->Set(String::NewFromUtf8(isolate, "action"), Integer::New(isolate, pathNode.action));
        nodeObj->Set(String::NewFromUtf8(isolate, "corridors"), corridorArr);

        int h = (currMinuteOfDay / 60) % 24;
        int m = currMinuteOfDay % 60;
        char c[6];
        sprintf(c, "%02d:%02d", h, m);
        nodeObj->Set(String::NewFromUtf8(isolate, "time"), String::NewFromUtf8(isolate, c));

        nodeObj->Set(String::NewFromUtf8(isolate, "duration"), Integer::New(isolate, duration));

        currMinuteOfDay += duration;
        pathNodesArr->Set(j, nodeObj);
      }

      pathObj->Set(String::NewFromUtf8(isolate, "path"), pathNodesArr);
      pathObj->Set(String::NewFromUtf8(isolate, "duration"), Integer::New(isolate, currMinuteOfDay - minuteOfDay));
      pathObj->Set(String::NewFromUtf8(isolate, "transit"), Integer::New(isolate, transitCount));
      pathObj->Set(String::NewFromUtf8(isolate, "summary"), String::NewFromUtf8(isolate, summary.c_str()));
      retVal->Set(i, pathObj);
    }

    return retVal;
  }
};
