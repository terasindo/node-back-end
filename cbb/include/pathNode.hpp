#pragma once

#include <algorithm>
#include <vector>

using namespace std;

class PathNode {
public:
  static const int ACTION_NONE = 0;
  static const int ACTION_GET_ON = 1;
  static const int ACTION_IN_BUS = 2;
  static const int ACTION_GET_OFF = 3;
  static const int ACTION_CROSS_BRIDGE = 4;

  int action;
  int busStop;
  vector<int> activeCorridors;
  vector<int> activeCorridorEnds;

  string time;
  int duration;

  PathNode(
      int _action,
      int _busStop,
      vector<int> _activeCorridors,
      vector<int> _activeCorridorEnds
    ) :
      action(_action),
      busStop(_busStop),
      activeCorridors(_activeCorridors),
      activeCorridorEnds(_activeCorridorEnds)
    {
  };
  ~PathNode() {
  };
};