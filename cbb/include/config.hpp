#pragma once

#include <cstdio>
#include <vector>
#include <algorithm>
#include <unordered_map>

using std::string;
using std::vector;
using std::unordered_map;

class Config {
private:
  string env;
  bool developmentMode;

public:
  Config(string _env) : env(_env) {
    developmentMode = (env == "development");
  }
  ~Config() {};

  bool isDevelopmentMode() {
    return developmentMode;
  }
};
