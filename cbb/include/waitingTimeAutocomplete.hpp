#pragma once

#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <set>
#include <vector>

#include "config.hpp"
#include "graph.hpp"

using namespace std;

class WaitingTimeAutocomplete {
private:
  bool isCorrupted;
  Graph *mGraph;
  Config *mConfig;
  short ***nextCommonBusStop;

public:
  WaitingTimeAutocomplete(Config* config, Graph* graph, string filePath) {
    mGraph = graph;
    mConfig = config;

    FILE *file = fopen(filePath.c_str(), "r");

    if (!file) {
      isCorrupted = true;
      return;
    }

    isCorrupted = false;

    map<int,int> l2gBlusStopIdx;
    map<int,int> l2gCorridorIdx;

    char buff[128];

    int nBusStop;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nBusStop) != 1);
    for (int i = 0; i < nBusStop; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string name = buff;
      l2gBlusStopIdx[i] = graph->getBusStopIdx(name);
    }
    int nCorridor;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nCorridor) != 1);
    for (int i = 0; i < nCorridor; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string name = buff;
      l2gCorridorIdx[i] = graph->getCorridorIdx(name);
    }

    nextCommonBusStop = new short**[nCorridor];
    for (int i = 0; i < nCorridor; i++) {
      nextCommonBusStop[i] = new short*[nBusStop];
      for (int j = 0; j < nBusStop; j++) {
        nextCommonBusStop[i][j] = new short[nBusStop];
      }
    }

    int nEntry;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nEntry) != 1);
    for (int i = 0; i < nEntry; i++) {
      int lCorridorIdx;
      int lFromIdx;
      int lToIdx;
      int lNextCommonBusStopIdx;
      isCorrupted = isCorrupted || (fscanf(file, "%d %d %d %d\n", &lCorridorIdx, &lFromIdx, &lToIdx, &lNextCommonBusStopIdx) != 4);

      int gCorridorIdx = l2gCorridorIdx[lCorridorIdx];
      int gFromIdx = l2gBlusStopIdx[lFromIdx];
      int gToIdx = l2gBlusStopIdx[lToIdx];
      int gNextCommonBusStopIdx = l2gBlusStopIdx[lNextCommonBusStopIdx];

      nextCommonBusStop[gCorridorIdx][gFromIdx][gToIdx] = gNextCommonBusStopIdx;
    }

    fclose(file);
  };
  ~WaitingTimeAutocomplete() {};

  bool isFileCorrupted() {
    return isCorrupted;
  }

  int get(int corridor, int from, int to) {
    return nextCommonBusStop[corridor][from][to];
  }
};
