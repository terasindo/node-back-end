#pragma once

#include <cstdio>
#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <set>
#include <vector>

#include "config.hpp"
#include "graph.hpp"

#include <node.h>
#include <v8.h>
#include <uv.h>

using v8::Local;
using v8::Object;
using v8::Isolate;
using v8::Number;
using v8::String;

using namespace std;

class TravelTime {
private:
  class TravelTimeBundle {
  public:
    int gFromIdx;
    int gToIdx;
    int minuteOfDay;
    vector<vector<char>> values;
  };

  const int NUMBERS_PER_ENTRY = 6;
  const int INDEX_MEAN = 0;
  const int INDEX_MEDIAN = 1;
  const int INDEX_P75 = 2;
  const int INDEX_P80 = 3;
  const int INDEX_P85 = 4;
  const int INDEX_P90 = 5;

  const int API_MINUTE_GRANULARITY = 30;
  const int MAX_TRAVEL_TIME = 90;

  // MEGA STRUCTURE: [dayIdx][minute][from..to] = [x, y, z]
  char ****data; // Using primitive structure for super speed
  int minuteGranularity;
  int defaultWindowDayIdx;

  // Mega structure supporting data
  int *windowDays;
  int *iWindowDays;
  int **edgeIdx;
  int *iEdgeIdxFrom;
  int *iEdgeIdxTo;

  bool isCorrupted;
  Graph *mGraph;
  Config *mConfig;

public:
  TravelTime(Config* config, Graph* graph, string filePath) {
    mGraph = graph;
    mConfig = config;

    FILE *file = fopen(filePath.c_str(), "r");

    if (!file) {
      isCorrupted = true;
      return;
    }

    isCorrupted = false;

    map<int,int> l2gBlusStopIdx;
    map<int,int> l2gCorridorIdx;

    char buff[128];

    int nBusStop;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nBusStop) != 1);
    for (int i = 0; i < nBusStop; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string name = buff;
      l2gBlusStopIdx[i] = graph->getBusStopIdx(name);
    }

    int nWindowDay;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nWindowDay) != 1);
    windowDays = new int[nWindowDay];
    for (int i = 0; i < nWindowDay; i++) {
      int day;
      isCorrupted = isCorrupted || (fscanf(file, "%d\n", &day) != 1);
      windowDays[i] = day;
    }

    // Set default window day to the longest
    defaultWindowDayIdx = nWindowDay - 1;

    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &minuteGranularity) != 1);

    // Temporary storage, will be destroyed automatically in the end of this constructor
    vector<TravelTimeBundle> travelTimeBundles;
    int nTravelTime;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nTravelTime) != 1);
    for (int i = 0; i < nTravelTime; i++) {
      int lFromIdx;
      int lToIdx;
      int minuteOfDay;
      isCorrupted = isCorrupted || (fscanf(file, "%d %d %d\n", &lFromIdx, &lToIdx, &minuteOfDay) != 3);

      TravelTimeBundle bundle;
      bundle.gToIdx = l2gBlusStopIdx[lToIdx];
      bundle.gFromIdx = l2gBlusStopIdx[lFromIdx];
      bundle.minuteOfDay = minuteOfDay;

      for (int j = 0; j < nWindowDay; j++) {
        vector<char> numsInDay;
        for (int k = 0; k < NUMBERS_PER_ENTRY; k++) {
          int x;
          isCorrupted = isCorrupted || (fscanf(file, "%d", &x) != 1);

          // Clean
          x = min(max(0, x), MAX_TRAVEL_TIME);
          if (x == 0) {
            x = MAX_TRAVEL_TIME; // Data unavailable
          }

          numsInDay.push_back(x);

          if (k + 1 < NUMBERS_PER_ENTRY) {
            isCorrupted = isCorrupted || (fscanf(file, " ") != 0);
          } else {
            isCorrupted = isCorrupted || (fscanf(file, "\n") != 0);
          }
        }
        bundle.values.push_back(numsInDay);
      }

      travelTimeBundles.push_back(bundle);
    }

    fclose(file);

    // Construct structure
    int maxWindowDay = 0;
    for (int i = 0; i < nWindowDay; i++) {
      maxWindowDay = max(maxWindowDay, windowDays[i]);
    }
    iWindowDays = new int[maxWindowDay + 1];
    for (int i = 0; i <= maxWindowDay; i++) {
      iWindowDays[i] = -1;
    }
    for (int i = 0; i < nWindowDay; i++) {
      iWindowDays[windowDays[i]] = i;
    }

    edgeIdx = new int*[nBusStop];
    for (int i = 0; i < nBusStop; i++) {
      edgeIdx[i] = new int[nBusStop];
      for (int j = 0; j < nBusStop; j++) {
        edgeIdx[i][j] = -1;
      }
    }
    int nEdge = 0;
    for (int i = 0; i < (int)travelTimeBundles.size(); i++) {
      int u = travelTimeBundles[i].gFromIdx;
      int v = travelTimeBundles[i].gToIdx;
      if (edgeIdx[u][v] == -1) {
        edgeIdx[u][v] = nEdge++;
      }
    }

    iEdgeIdxFrom = new int[nEdge];
    iEdgeIdxTo = new int[nEdge];
    for (int i = 0; i < (int)travelTimeBundles.size(); i++) {
      int u = travelTimeBundles[i].gFromIdx;
      int v = travelTimeBundles[i].gToIdx;

      int p = edgeIdx[u][v];
      iEdgeIdxFrom[p] = u;
      iEdgeIdxTo[p] = v;
    }

    // Construct mega structure
    data = new char***[nWindowDay];
    for (int d = 0; d < nWindowDay; d++) {
      int sz = 24 * 60 / minuteGranularity;
      data[d] = new char**[sz];

      for (int t = 0; t < sz; t++) {
        data[d][t] = new char*[nEdge];
      }
    }

    // Fill mega structure
    for (int i = 0; i < (int)travelTimeBundles.size(); i++) {
      int u = travelTimeBundles[i].gFromIdx;
      int v = travelTimeBundles[i].gToIdx;
      int t = travelTimeBundles[i].minuteOfDay / minuteGranularity;

      vector<vector<char>> &values = travelTimeBundles[i].values;
      for (int day = 0; day < nWindowDay; day++) {
        int p = edgeIdx[u][v];
        data[day][t][p] = new char[6]{
            values[day][INDEX_MEAN],
            values[day][INDEX_MEDIAN],
            values[day][INDEX_P75],
            values[day][INDEX_P80],
            values[day][INDEX_P85],
            values[day][INDEX_P90],

        };
      }
    }
  };
  ~TravelTime() {};

  bool isFileCorrupted() {
    return isCorrupted;
  }

  char* getDataAddress(int from, int to, int minuteOfDay) {
    int t = minuteOfDay / minuteGranularity;
    int p = edgeIdx[from][to];

    if (mConfig->isDevelopmentMode()) {
      printf(
          "Request for '%s' => '%s'\n",
          mGraph->getBusStopName(iEdgeIdxFrom[p]).c_str(),
          mGraph->getBusStopName(iEdgeIdxTo[p]).c_str()
      );
    }

    return data[defaultWindowDayIdx][t][p];
  }

  int getMedian(int from, int to, int minuteOfDay) {
    return getDataAddress(from, to, minuteOfDay)[INDEX_MEDIAN];
  }

  int getMean(int from, int to, int minuteOfDay) {
    return getDataAddress(from, to, minuteOfDay)[INDEX_MEAN];
  }

  int getP75(int from, int destination, int minuteOfDay) {
    return getDataAddress(from, destination, minuteOfDay)[INDEX_P75];
  }

  int getP80(int from, int destination, int minuteOfDay) {
    return getDataAddress(from, destination, minuteOfDay)[INDEX_P80];
  }

  int getP85(int from, int destination, int minuteOfDay) {
    return getDataAddress(from, destination, minuteOfDay)[INDEX_P85];
  }

  int getP90(int from, int destination, int minuteOfDay) {
    return getDataAddress(from, destination, minuteOfDay)[INDEX_P90];
  }

  Local<Object> getMeasurement(Isolate* &isolate, int from, int destination, int minuteOfDay) {
    int mean = getMean(from, destination, minuteOfDay);
    int median = getMedian(from, destination, minuteOfDay);
    int p75 = getP75(from, destination, minuteOfDay);
    int p80 = getP80(from, destination, minuteOfDay);
    int p85 = getP85(from, destination, minuteOfDay);
    int p90 = getP90(from, destination, minuteOfDay);

    Local<Object> result = Object::New(isolate);

    result->Set(String::NewFromUtf8(isolate, "mean"), Number::New(isolate, mean));
    result->Set(String::NewFromUtf8(isolate, "median"), Number::New(isolate, median));
    result->Set(String::NewFromUtf8(isolate, "p75"), Number::New(isolate, p75));
    result->Set(String::NewFromUtf8(isolate, "p80"), Number::New(isolate, p80));
    result->Set(String::NewFromUtf8(isolate, "p85"), Number::New(isolate, p85));
    result->Set(String::NewFromUtf8(isolate, "p90"), Number::New(isolate, p90));

    return result;
  }

  /*
   * {
   *   minuteOfDays: [0, 10, 20, 30, ..., 1430],
   *   data: [
   *     {
   *       from: "Harmoni",
   *       to: "Olimo",
   *       mean: [5, 4, 5, ...],
   *       p90: [9, 9, 8, ...]
   *     }
   *   ]
   * }
   */
  Local<Object> getAllEdge(Isolate* &isolate, int corridor) {
    Local<Object> result = Object::New(isolate);

    // Put minuteOfDays
    Local<Array> minuteOfDays = Array::New(isolate);
    for (int i = 0; i*API_MINUTE_GRANULARITY < 24*60; i++) {
      minuteOfDays->Set(i, Integer::New(isolate, i * API_MINUTE_GRANULARITY));
    }

    // Get all common bus stops
    const vector<pair<BusStop*,BusStop*>> &edges = mGraph->getEdgesInCorridor(corridor);

    // Put data
    Local<Array> data = Array::New(isolate);
    for (int i = 0; i < (int)edges.size(); i++) {
      Local<Object> record = Object::New(isolate);

      Local<Array> means = Array::New(isolate);
      Local<Array> medians = Array::New(isolate);
      Local<Array> p75s = Array::New(isolate);
      Local<Array> p80s = Array::New(isolate);
      Local<Array> p85s = Array::New(isolate);
      Local<Array> p90s = Array::New(isolate);
      for (int j = 0; j*API_MINUTE_GRANULARITY < 24*60; j++) {
        char* item = getDataAddress(edges[i].first->index, edges[i].second->index, j*API_MINUTE_GRANULARITY);

        means->Set(j, Integer::New(isolate, item[INDEX_MEAN]));
        medians->Set(j, Integer::New(isolate, item[INDEX_MEDIAN]));
        p75s->Set(j, Integer::New(isolate, item[INDEX_P75]));
        p80s->Set(j, Integer::New(isolate, item[INDEX_P80]));
        p85s->Set(j, Integer::New(isolate, item[INDEX_P85]));
        p90s->Set(j, Integer::New(isolate, item[INDEX_P90]));
      }

      record->Set(String::NewFromUtf8(isolate, "from"), String::NewFromUtf8(isolate, edges[i].first->name.c_str()));
      record->Set(String::NewFromUtf8(isolate, "to"), String::NewFromUtf8(isolate, edges[i].second->name.c_str()));
      record->Set(String::NewFromUtf8(isolate, "mean"), means);
      record->Set(String::NewFromUtf8(isolate, "median"), medians);
      record->Set(String::NewFromUtf8(isolate, "p75"), p75s);
      record->Set(String::NewFromUtf8(isolate, "p80"), p80s);
      record->Set(String::NewFromUtf8(isolate, "p85"), p85s);
      record->Set(String::NewFromUtf8(isolate, "p90"), p90s);

      data->Set(i, record);
    }

    result->Set(String::NewFromUtf8(isolate, "minuteOfDays"), minuteOfDays);
    result->Set(String::NewFromUtf8(isolate, "data"), data);
    return result;
  }
};
