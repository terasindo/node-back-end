#pragma once

#include <vector>
#include <string>
#include <utility>
#include <unordered_map>
#include <unordered_set>

using std::pair;
using std::make_pair;
using std::string;
using std::vector;
using std::unordered_map;
using std::unordered_set;

class Corridor;
class BusStop {
public:
  int index;
  string name;
  string slug;
  vector<string> aliases;
  double latitude;
  double longitude;
  bool isTerminal;
  bool isCommon;
  vector<BusStop*> bridges;
  unordered_map<Corridor*, unordered_map<BusStop*, BusStop*>> next;
  unordered_set<Corridor*> corridors; // Corridors it serves

  BusStop(
      int _index,
      string _name,
      string _slug,
      vector<string> _aliases,
      double _latitude,
      double _longitude,
      bool _isTerminal,
      bool _isCommon
    ) :
      index(_index),
      name(_name),
      slug(_slug),
      aliases(_aliases),
      latitude(_latitude),
      longitude(_longitude),
      isTerminal(_isTerminal),
      isCommon(_isCommon)
    {
  }

  ~BusStop () {
    aliases.clear();
    bridges.clear();
    next.clear();
    corridors.clear();
  }

  BusStop* getNext(Corridor* corridor, BusStop* destinationBusStop) {
    // printf("Here at '%s' attempt: '%s'\n", name.c_str(), destinationBusStop->name.c_str());
    if (next.count(corridor) == 0) {
      return NULL;
    }

    if (next[corridor].count(destinationBusStop) == 0) {
      return NULL;
    }

    return next[corridor][destinationBusStop];
  }
};
