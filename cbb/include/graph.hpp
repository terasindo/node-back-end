#pragma once

#include <cstdio>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <map>

#include "busStop.hpp"
#include "corridor.hpp"

using std::string;
using std::vector;
using std::unordered_map;

class Graph {
private:
  vector<BusStop*> busStops;
  vector<Corridor*> corridors;
  vector<vector<pair<BusStop*,BusStop*>>> edgesByCorridor;
  unordered_map<string,int> busStopNameToIdx;
  unordered_map<string,int> corridorNameToIdx;
  bool isCorrupted;

  // For super speed, we create these (idx -> name) mapping
  vector<string> busStopNames;
  vector<string> corridorNames;

public:
  Graph(string filePath) {
    FILE *file = fopen(filePath.c_str(), "r+");

    if (!file) {
      isCorrupted = true;
      return;
    }

    isCorrupted = false;

    char buff[1024];

    int nBusStop;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nBusStop) != 1);
    for (int i = 0; i < nBusStop; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);

      string name = buff;

      int nAlias;
      double latitude;
      double longitude;
      int isTerminal;
      int isCommon;
      isCorrupted = isCorrupted || (fscanf(file, "%d %lf %lf %d %d %s\n", &nAlias, &latitude, &longitude, &isTerminal, &isCommon, buff) != 6);

      string slug = buff;

      vector<string> aliases;
      for (int j = 0; j < nAlias; j++) {
        isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
        string alias = buff;
        aliases.push_back(alias);
      }

      BusStop* busStop = new BusStop(i, name, slug, aliases, latitude, longitude, isTerminal == 1, isCommon == 1);
      busStops.push_back(busStop);
      busStopNames.push_back(name);
    }

    int nAllBridge;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nAllBridge) != 1);
    for (int i = 0; i < nAllBridge; i++) {
      int u, v;
      isCorrupted = isCorrupted || (fscanf(file, "%d %d\n", &u, &v) != 2);
      (busStops[u]->bridges).push_back(busStops[v]);
    }

    int nCorridor;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nCorridor) != 1);
    for (int i = 0; i < nCorridor; i++) {
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string name = buff;
      isCorrupted = isCorrupted || (fscanf(file, "%[^\n]\n", buff) != 1);
      string fullName = buff;

      int nPointPolylineFromStart;
      int nPointPolylineFromEnd;
      int startBusStopId;
      int endBusStopId;
      isCorrupted = isCorrupted || (fscanf(file, "%d %d %d %d %s\n", &nPointPolylineFromStart, &nPointPolylineFromEnd, &startBusStopId, &endBusStopId, buff) != 5);
      string slug = buff;

      for (int j = 0; j < nPointPolylineFromStart; j++) {
        double latitude;
        double longitude;
        isCorrupted = isCorrupted || (fscanf(file, "%lf %lf", &latitude, &longitude) != 2);
      }
      isCorrupted = isCorrupted || (fscanf(file, "\n") != 0);

      for (int j = 0; j < nPointPolylineFromEnd; j++) {
        double latitude;
        double longitude;
        isCorrupted = isCorrupted || (fscanf(file, "%lf %lf", &latitude, &longitude) != 2);
      }
      isCorrupted = isCorrupted || (fscanf(file, "\n") != 0);

      Corridor* corridor = new Corridor(i, name, slug, fullName, busStops[startBusStopId], busStops[endBusStopId]);
      corridors.push_back(corridor);
      corridorNames.push_back(name);
    }

    int nEdge;
    isCorrupted = isCorrupted || (fscanf(file, "%d\n", &nEdge) != 1);
    for (int i = 0; i < nEdge; i++) {
      int fromBusStopId;
      int toBusStopId;
      int corridorId;
      int destinationBusStopId;
      isCorrupted = isCorrupted || (fscanf(file, "%d %d %d %d\n", &fromBusStopId, &toBusStopId, &corridorId, &destinationBusStopId) != 4);

      Corridor* corridor = corridors[corridorId];
      BusStop* fromBusStop = busStops[fromBusStopId];
      BusStop* toBusStop = busStops[toBusStopId];
      BusStop* destinationBusStop = busStops[destinationBusStopId];

      // Attach the edge
      fromBusStop->next[corridor][destinationBusStop] = toBusStop;
      fromBusStop->corridors.insert(corridor);
    }

    fclose(file);

    // Fill mapping
    for (int i = 0; i < (int)busStops.size(); i++) {
      busStopNameToIdx[busStops[i]->name] = i;
    }
    for (int i = 0; i < (int)corridors.size(); i++) {
      corridorNameToIdx[corridors[i]->name] = i;
    }

    // Fill edges
    edgesByCorridor.clear();
    for (Corridor* c : corridors) {
      vector<pair<BusStop*,BusStop*>> edgesInCorridor;
      BusStop* curr = c->startBusStop;

      // Forward
      while (curr != c->endBusStop) {
        BusStop* next = curr->getNext(c, c->endBusStop);
        edgesInCorridor.push_back(make_pair(curr, next));

        curr = next;
      }

      // Backward
      while (curr != c->startBusStop) {
        BusStop* next = curr->getNext(c, c->startBusStop);
        edgesInCorridor.push_back(make_pair(curr, next));

        curr = next;
      }

      edgesByCorridor.push_back(edgesInCorridor);
    }
  }

  ~Graph() {};

  bool isFileCorrupted() {
    return isCorrupted;
  }

  int getBusStopIdx(string &name) {
    unordered_map<string,int>::const_iterator it = busStopNameToIdx.find(name);
    if (it == busStopNameToIdx.end()) {
      return -1;
    }
    return it->second;
  }
  int getCorridorIdx(string name) {
    unordered_map<string,int>::const_iterator it = corridorNameToIdx.find(name);
    if (it == corridorNameToIdx.end()) {
      return -1;
    }
    return it->second;
  }

  string getBusStopName(int idx) {
    return busStopNames[idx];
  }
  string getCorridorName(int idx) {
    return corridorNames[idx];
  }

  double getDistance(int busStopA, int busStopB) {
    double dx = busStops[busStopA]->latitude - busStops[busStopB]->latitude;
    double dy = busStops[busStopA]->longitude - busStops[busStopB]->longitude;
    return sqrt(dx*dx + dy*dy);
  }

  vector<int> getCommonBusStopIdxs(int corridorIdx, int busStopIdx) {
    vector<int> result;

    BusStop* curr;
    Corridor* corridor = corridors[corridorIdx];

    // To corridor start
    curr = busStops[busStopIdx]->getNext(corridor, corridor->startBusStop);
    while (curr != NULL) {
      if (curr->isCommon) {
        result.push_back(curr->index);
      }
      curr = curr->getNext(corridor, corridor->startBusStop);
    }

    // Reverse to preserve natural order
    reverse(result.begin(), result.end());

    // To corridor end
    curr = busStops[busStopIdx]->getNext(corridor, corridor->endBusStop);
    while (curr != NULL) {
      if (curr->isCommon) {
        result.push_back(curr->index);
      }
      curr = curr->getNext(corridor, corridor->endBusStop);
    }
    return result;
  }

  vector<int> getAddedCommonBusStopIdxs(int corridorIdx, int busStopIdx) {
    vector<int> result;

    BusStop* curr;
    Corridor* corridor = corridors[corridorIdx];

    // To corridor start
    curr = busStops[busStopIdx]->getNext(corridor, corridor->startBusStop);
    while (curr != NULL) {
      if (curr->isCommon) {
        result.push_back(curr->index);
      }
      curr = curr->getNext(corridor, corridor->startBusStop);
    }

    // Reverse to preserve natural order
    reverse(result.begin(), result.end());

    // Add the current bus stop!
    result.push_back(busStopIdx);

    // To corridor end
    curr = busStops[busStopIdx]->getNext(corridor, corridor->endBusStop);
    while (curr != NULL) {
      if (curr->isCommon) {
        result.push_back(curr->index);
      }
      curr = curr->getNext(corridor, corridor->endBusStop);
    }
    return result;
  }

  vector<pair<BusStop*,BusStop*>> getEdgesInCorridor(int corridorIdx) {
    return edgesByCorridor[corridorIdx];
  }
};
