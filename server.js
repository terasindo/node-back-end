'use strict';

/**
 * Server start up script
 */

var http = require('http');
var power = require('./power');

module.exports = function () {
  return power.on()
    .then(function loadApp() {
      var app = require('./app');

      var port = process.env.PORT || '3000';
      app.set('port', port);

      var server = http.createServer(app);
      server.listen(port);

      // Report for events
      server.on('error', console.error);
      server.on('listening', function () {
        var address = server.address();
        console.log('Listening on', address);
      });
    });
};

// Is called from command line?
if (!module.parent) {
  module.exports();
}

